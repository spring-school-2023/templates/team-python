import pytest
from hamcrest import assert_that, equal_to

from warehouse_system.hello_world import HelloWorld

def test_hello_should_return_world_classic():
    under_test = HelloWorld()

    # different assertion styles (just use one :) )
    # classic
    assert "world" == under_test.hello()
    # hamcrest
    assert_that(under_test.hello(), equal_to("world"))


@pytest.mark.parametrize("name", ["fred", "ian", "paul"])
def test_greet_should_return_world_classic(name):
    under_test = HelloWorld()

    # different assertion styles (just use one :) )
    # classic
    assert f"hello {name}" == under_test.greet(name)
    # hamcrest
    assert_that(under_test.greet(name), equal_to(f"hello {name}"))
from warehouse_system.hello_world import HelloWorld

WAREHOUSE_API_URL = "http://springschool-lb-54580289.eu-central-1.elb.amazonaws.com/"

def print_hello_world():
    print("Hello world.")

if __name__ == '__main__':
    print_hello_world()
    hello_world = HelloWorld()
    print(hello_world.greet("user"))

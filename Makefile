.PHONY: setup
setup:
	PIPENV_VENV_IN_PROJECT=1 pipenv install --dev

.PHONY: requirements
requirements:
	pipenv sync
	pipenv run pip freeze > requirements.txt

.PHONY: clean
clean:
	pipenv --rm
	rm -rf ./coverage/

.PHONY: test
test:
	pipenv run python3 -m pytest --junitxml=./coverage/junit.xml

.PHONY: coverage
coverage:
	pipenv run coverage run --data-file=./coverage/.coverage -m pytest --junitxml=./coverage/junit.xml
	pipenv run coverage report --data-file=./coverage/.coverage 
	pipenv run coverage xml --data-file=./coverage/.coverage -o ./coverage/coverage.xml

all: setup coverage